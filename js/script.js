document.addEventListener('DOMContentLoaded', () => {

    'use strict'

    // function onEntry(entry) {
    //     entry.forEach(change => {
    //         if (change.isIntersecting) {
    //             change.target.classList.add('element-show');
    //         }
    //     });
    // }

    // let options = { threshold: [0.7] };
    // let observer = new IntersectionObserver(onEntry, options);
    // let elements = document.querySelectorAll('.element-animation');
        
    // for (let elm of elements) {
    //     observer.observe(elm);
    // }

   const animatedElement = ScrollReveal({
       origin: 'top',
       distance: '60px',
       duration: 2500,
       delay: 400,
   })

   animatedElement.reveal(`.intro-element-animation`, {delay: 700, origin: 'left'})
   animatedElement.reveal(`.first-element-animation`, {viewFactor: 0.6})
   animatedElement.reveal(`.element-animation`)
    
});